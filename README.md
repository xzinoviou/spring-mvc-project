A Spring MVC 5.2.0.RELEASE backbone project.

Config Method : code config.
Connection Pooling : Hikari
DB Migration : flyway
DB : MySQL 8

Anyone can use this as a backbone for instant local development projects.
In case you want ot change the DB or CP vendors , then changes should be made in the application.settings file and in the configuration files ,located in the classpath & in config packages.