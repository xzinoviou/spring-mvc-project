package com.xzinoviou.spring.mvc.project;

import com.xzinoviou.spring.mvc.project.config.AppConfig;
import com.xzinoviou.spring.mvc.project.config.JpaConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/** @author xzinoviou created 9/3/20 */
public class WebAppInitializer implements WebApplicationInitializer {

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();

    context.register(AppConfig.class, JpaConfig.class);

    servletContext.addListener(new ContextLoaderListener(context));

    ServletRegistration.Dynamic dispatcherServlet =
        servletContext.addServlet("dispatcherServlet", new DispatcherServlet(context));

    dispatcherServlet.setLoadOnStartup(1);
    dispatcherServlet.addMapping("/");
  }
}
