package com.xzinoviou.spring.mvc.project.config;

import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/** @author xzinoviou created 9/3/20 */
@Configuration
@EnableWebMvc
@ComponentScan(
    basePackages = {
      "com.xzinoviou.spring.mvc.project.model",
      "com.xzinoviou.spring.mvc.project.dao",
      "com.xzinoviou.spring.mvc.project.controller"
    })
@EnableAspectJAutoProxy
@PropertySource(value = "classpath:application.properties")
public class AppConfig {

  @Bean
  InternalResourceViewResolver viewResolver() {
    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
    viewResolver.setPrefix("/WEB-INF/views/");
    viewResolver.setSuffix(".jsp");
    return viewResolver;
  }
}
