package com.xzinoviou.spring.mvc.project.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Objects;
import java.util.Properties;

/** @author xzinoviou created 9/3/20 */
@Configuration
@ComponentScan(basePackages = {"com.xzinoviou.spring.mvc.project.service"})
@EnableTransactionManagement
@PropertySource(value = "classpath:application.properties")
public class JpaConfig {

  @Autowired Environment env;

  @Bean(destroyMethod = "close")
  HikariDataSource dataSource() {
    HikariConfig config = new HikariConfig();
    config.setPoolName(env.getRequiredProperty("hikari.poolName"));
    config.setIdleTimeout(Integer.parseInt(env.getRequiredProperty("hikari.idleTimeout")));
    config.setDriverClassName(env.getRequiredProperty("db.driver"));
    config.setJdbcUrl(env.getRequiredProperty("db.url"));
    config.setUsername(env.getRequiredProperty("db.username"));
    config.setPassword(env.getRequiredProperty("db.password"));
    config.setMaximumPoolSize(env.getRequiredProperty("hikari.maximumPoolSize", Integer.class));

    config.addDataSourceProperty(
        "hikari.dataSourceClassName", env.getRequiredProperty("hikari.dataSourceClassName"));
    config.addDataSourceProperty(
        "hikari.cachePrepStmts", env.getRequiredProperty("hikari.cachePrepStmts"));
    config.addDataSourceProperty(
        "hikari.prepStmtCacheSize",
        Integer.parseInt(env.getRequiredProperty("hikari.prepStmtCacheSize")));
    config.addDataSourceProperty(
        "hikari.prepStmtCacheSqlLimit",
        Integer.parseInt(env.getRequiredProperty("hikari.prepStmtCacheSqlLimit")));
    config.addDataSourceProperty(
        "hikari.useServerPrepStmts", env.getRequiredProperty("hikari.useServerPrepStmts"));

    return new HikariDataSource(config);
  }

  @Bean(initMethod = "migrate")
  Flyway flyway() {
    return Flyway.configure()
        .dataSource(dataSource())
        .baselineOnMigrate(true)
        .locations("/flyway")
        .load();
  }

  @Bean
  @DependsOn(value = "flyway")
  LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    sessionFactory.setDataSource(dataSource());
    sessionFactory.setPackagesToScan("com.xzinoviou.msc.infosec.model");
    sessionFactory.setHibernateProperties(hibernateProperties());
    return sessionFactory;
  }

  private Properties hibernateProperties() {
    Properties props = new Properties();
    props.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
    props.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));
    props.put(
        "hibernate.ejb.naming_strategy", env.getRequiredProperty("hibernate.ejb.naming_strategy"));
    props.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
    props.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));
    return props;
  }

  @Bean
  TransactionManager transactionManager() {
    return new HibernateTransactionManager(Objects.requireNonNull(sessionFactory().getObject()));
  }
}
